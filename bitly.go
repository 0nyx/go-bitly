// Copyright 2018 0nyx a.k.a Nikolay Kotykhov
//
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package bitly

import (
	"bytes"
	"context"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

const (
	defaultBaseURL   = "https://api-ssl.bitly.com/v4/"
	defaultUserAgent = "bitcli"

	mediaTypeV4 = "application/json"
)

type endpoint struct {
	client *Client
}

type Serializable interface {
	SerializeJSON() ([]byte, error)
}

type Deserializable interface {
	ParseJSON([]byte) error
}

type Client struct {
	client *http.Client // HTTP client used to communicate with the API

	// Base URL for API requests. Defaults to public Bitly API, but can be
	// set to a test domain or a gateway. BaseURL should
	// always be specified with a trailing slash.
	BaseURL *url.URL

	// User agent used when communicating with the Bitly API.
	UserAgent string

	// Endpoints has nothing but reference to this object
	// to avoid extra allocations we going to reuse common
	common endpoint

	User     *UserEndpoint
	Groups   *GroupsEndpoint
	Bitlinks *BitlinksEndpoint
}

func allocateEndpoints(c *Client) {
	c.common.client = c
	c.User = (*UserEndpoint)(&c.common)
	c.Groups = (*GroupsEndpoint)(&c.common)
	c.Bitlinks = (*BitlinksEndpoint)(&c.common)
}

// NewClient returns a new Bitly API client, if a nil given http.defaultClient
// will be used instead
func NewClient(c *http.Client) *Client {
	if c == nil {
		c = http.DefaultClient
	}
	// we can ignore error since we control the constant
	// and can ensure that it's proper URL
	baseUrl, _ := url.Parse(defaultBaseURL)
	cli := &Client{client: c, BaseURL: baseUrl, UserAgent: defaultUserAgent}
	allocateEndpoints(cli)
	return cli
}

// NewCustomClient returns a new Bitly API client configured to point
// to custom API endpoint. if httpCli is nil default HTTP client will
// be used instead
func NewCustomClient(urlStr string, httpCli *http.Client) (cli *Client, err error) {
	baseUrl, err := url.Parse(urlStr)
	if err != nil {
		return nil, err
	}
	// enforce BaseUrl rule
	if !strings.HasSuffix(baseUrl.Path, "/") {
		baseUrl.Path += "/"
	}

	if httpCli == nil {
		httpCli = http.DefaultClient
	}

	cli = &Client{BaseURL: baseUrl, client: httpCli, UserAgent: defaultUserAgent}
	allocateEndpoints(cli)
	return
}

// NewRequest creates new http.Request ready to use by client
func (cli *Client) NewRequest(method, path string, body Serializable) (*http.Request, error) {
	if !strings.HasSuffix(cli.BaseURL.Path, "/") {
		return nil, errors.New("BaseURL must have a trailing slash")
	}

	u, err := cli.BaseURL.Parse(path)
	if err != nil {
		return nil, err
	}

	var buf io.Reader
	// Encode body to JSON if given
	if body != nil {
		bdy, err := body.SerializeJSON()
		buf = bytes.NewReader(bdy)
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, u.String(), buf)

	if err != nil {
		return nil, err
	}

	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	req.Header.Set("Accept", mediaTypeV4)

	if cli.UserAgent != "" {
		req.Header.Set("User-Agent", cli.UserAgent)
	}

	return req, nil
}

type Response struct {
	HTTPResponse *http.Response

	NextPage int
}

// Do performs http request req using ctx
func (cli *Client) Do(ctx context.Context, req *http.Request, v interface{}) (response *Response, err error) {
	if req == nil {
		err = errors.New("Cli: nil request")
		return nil, err
	}

	req = req.WithContext(ctx)
	resp, err := cli.client.Do(req)

	if err != nil {
		// check if context has been cancled
		// it may have more useful information
		// than error in most cases
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}

		return nil, err
	}

	defer resp.Body.Close()

	response = new(Response)
	response.HTTPResponse = resp

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			io.Copy(w, resp.Body)
		} else if ds, ok := v.(Deserializable); ok {
			buf, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			err = ds.ParseJSON(buf)
			if err != nil {
				return nil, err
			}
		}
	}

	return response, nil
}

// intToStr is a short-cut function
// to avoid extra includes in other files
func itoa(i int) string {
	return strconv.Itoa(i)
}

func atoi(s []byte) (int, error) {
	return strconv.Atoi(string(s))
}
