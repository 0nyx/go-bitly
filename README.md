Go-Bitly
========

Golang Bitly.com APIv4 client heavily inspired by [Google's GitHub API client](https://github.com/google/go-github).


Author
======

Copyright (C) 2018 0nyx aka Nikolay Kotykhov
