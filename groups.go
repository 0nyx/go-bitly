// Copyright 2018 0nyx a.k.a Nikolay Kotykhov
//
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package bitly

import (
	"context"
	"fmt"
)

type GroupsEndpoint endpoint

type GroupPreferences struct {
	GroupGuid        string `json:"group_guid"`
	DomainPreference string `json:"domain_preference"`
}

func (ep *GroupsEndpoint) Countries(ctx context.Context, groupGuid string, opts *MetricsOptions) (*ClickMetrics, *Response, error) {
	var p string
	if opts != nil {
		p = fmt.Sprintf("groups/%s/countries?%s", groupGuid, opts.Encode())
	} else {
		p = fmt.Sprintf("groups/%s/countries", groupGuid)
	}
	req, err := ep.client.NewRequest("GET", p, nil)
	if err != nil {
		return nil, nil, err
	}
	metrics := new(ClickMetrics)
	resp, err := ep.client.Do(ctx, req, metrics)
	return metrics, resp, err
}

type GroupBitlinksOptions struct {
	List *ListOptions
}

func (opts *GroupBitlinksOptions) Encode() string {
	if opts.List != nil {
		return opts.List.Encode()
	}
	return ""
}

func (ep *GroupsEndpoint) Bitlinks(ctx context.Context, groupGuid string, opts *GroupBitlinksOptions) (*BitlinkListResponse, *Response, error) {
	var p string
	if opts != nil {
		p = fmt.Sprintf("groups/%s/bitlinks?%s", groupGuid, opts.Encode())
	} else {
		p = fmt.Sprintf("groups/%s/bitlinks", groupGuid)
	}
	req, err := ep.client.NewRequest("GET", p, nil)
	if err != nil {
		return nil, nil, err
	}
	var list = new(BitlinkListResponse)
	resp, err := ep.client.Do(ctx, req, list)

	return list, resp, err
}
