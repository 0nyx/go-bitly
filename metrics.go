package bitly

import (
	"encoding/json"
	"gitlab.com/0nyx/iso8601"
	"net/url"
	"time"
)

const (
	maxClickMetricsTokens = 4098
)

type ClickMetric struct {
	Clicks int    `json:"clicks"`
	Value  string `json:"value"`
}

type ClickMetrics struct {
	Unit          string        `json:"unit"`
	Units         int           `json:"units"`
	UnitReference iso8601.Time  `json:"unit_reference"`
	Facet         string        `json:"facet"`
	Metrics       []ClickMetric `json:"metrics"`
}

func (m *ClickMetrics) ParseJSON(js []byte) error {
	return json.Unmarshal(js, m)
}

type MetricUnit string

const (
	UnitMinute MetricUnit = "minute"
	UnitHour   MetricUnit = "hour"
	UnitDay    MetricUnit = "day"
	UnitWeek   MetricUnit = "week"
	UnitMonth  MetricUnit = "month"

	defaultUnits = -1
)

func (u MetricUnit) Valid() bool {
	switch u {
	case UnitMinute, UnitHour, UnitDay, UnitWeek, UnitMonth:
		return true
	default:
	}

	return false
}

type MetricsOptions struct {
	Unit          MetricUnit
	Units         int
	UnitReference *time.Time
	Facet         string
}

func (opts *MetricsOptions) Encode() string {
	values := url.Values{}
	if opts.Unit.Valid() {
		values.Add("unit", string(opts.Unit))
	}
	if opts.Units != defaultUnits {
		values.Add("units", itoa(opts.Units))
	}
	if opts.Facet != "" {
		values.Add("facet", opts.Facet)
	}
	if opts.UnitReference != nil {
		values.Add("unit_reference", iso8601.String(*opts.UnitReference))
	}

	return values.Encode()
}
