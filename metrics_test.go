package bitly

import (
	"gitlab.com/0nyx/iso8601"
	"testing"
)

func cmpMetrics(m1, m2 ClickMetrics, t *testing.T) {
	if m1.Unit != m2.Unit {
		t.Errorf("Unit parse error. Expected: %s, Got: %s", m1.Unit, m2.Unit)
	}
	if m1.Units != m2.Units {
		t.Errorf("Units parse error. Expected: %s, Got: %s", m1.Units, m2.Units)
	}

	if m1.Facet != m2.Facet {
		t.Errorf("Facet parse error. Expected: %s, Got: %s", m1.Facet, m2.Facet)
	}

	if len(m1.Metrics) != len(m2.Metrics) {
		t.Errorf("Facet parse error. Expected: %+v, Got: %+v", m1.Metrics, m2.Metrics)
	}

	for i, m := range m1.Metrics {
		if m.Clicks != m2.Metrics[i].Clicks || m.Value != m2.Metrics[i].Value {
			t.Errorf("Facet parse error. Expected: %+v, Got: %+v", m1.Metrics, m2.Metrics)
			break
		}
	}
}

func TestClicksMetricJsonParse(t *testing.T) {
	var validTest = []byte(`{"units": 30, "unit": "day", "unit_reference": "2018-12-11T01:18:50+0000", "facet": "countries", "metrics":[
                            {
                              "clicks": 10,
                              "value": "US",
                            },
                            {
                              "clicks": 99,
                              "value": "GB"
                            }
                          ]}`)
	var parsed ClickMetrics
	err := parsed.ParseJSON(validTest)
	if err != nil {
		t.Errorf("Test cought error, when was not expecting. %q", err)
	}

	timestamp, _ := iso8601.ParseTime("2018-12-11T01:18:50+0000")
	var target = ClickMetrics{
		Units:         30,
		Unit:          "day",
		UnitReference: timestamp,
		Facet:         "countries",
		Metrics: []ClickMetric{
			{
				Clicks: 10,
				Value:  "US",
			},
			{
				Clicks: 99,
				Value:  "GB",
			},
		},
	}

	cmpMetrics(target, parsed, t)
}
