package main

import (
	"context"
	"fmt"
	"gitlab.com/0nyx/go-bitly"
	"net/http"
	"os"
)

type TokenAuthTransport struct {
	transport http.RoundTripper
	token     string
}

func NewTokenAuthTransport(original http.RoundTripper, token string) *TokenAuthTransport {
	return &TokenAuthTransport{
		transport: original,
		token:     token,
	}
}
func (t *TokenAuthTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("Authorization", "Bearer "+t.token)
	res, err := t.transport.RoundTrip(req)
	return res, err
}

func main() {
	token := os.Getenv("BITLY_TOKEN")
	if token == "" {
		panic("auth token is not set")
	}
	tokenAuthTrans := NewTokenAuthTransport(http.DefaultTransport, token)
	cli := bitly.NewClient(&http.Client{Transport: tokenAuthTrans})

	user, _, err := cli.User.Get(context.Background())

	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", user)
}
