package bitly

import "net/url"

type Pagination struct {
	Size  int    `json:"size"`
	Page  int    `json:"page"`
	Total int    `json:"total"`
	Prev  string `json:"prev"`
	Next  string `json:"next"`
}

type ListOptions struct {
	Size int
	Page int
}

func (opts ListOptions) Encode() string {
	values := url.Values{}
	if opts.Size >= 0 {
		values.Add("size", itoa(opts.Size))
	}
	if opts.Page > 0 {
		values.Add("page", itoa(opts.Page))
	}

	return values.Encode()
}
