// Copyright 2018 0nyx a.k.a Nikolay Kotykhov
//
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package bitly

import (
	"context"
	"encoding/json"
	"fmt"
)

type Bitlink struct {
	Id string `json: "id"`
}

type BitlinkListResponse struct {
	Pagination `json:"pagination"`
	Links      []Bitlink `json:"links"`
}

func (r *BitlinkListResponse) ParseJSON(js []byte) (err error) {
	return json.Unmarshal(js, r)
}

type BitlinksEndpoint endpoint

func (ep *BitlinksEndpoint) Countries(ctx context.Context, groupGuid string, opts *MetricsOptions) (*ClickMetrics, *Response, error) {
	var p string
	if opts != nil {
		p = fmt.Sprintf("bitlinks/%s/countries?%s", groupGuid, opts.Encode())
	} else {
		p = fmt.Sprintf("bitlinks/%s/countries", groupGuid)
	}
	req, err := ep.client.NewRequest("GET", p, nil)
	if err != nil {
		return nil, nil, err
	}
	metrics := new(ClickMetrics)
	resp, err := ep.client.Do(ctx, req, metrics)
	return metrics, resp, err
}
