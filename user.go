// Copyright 2018 0nyx a.k.a Nikolay Kotykhov
//
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package bitly

import (
	"context"
	"encoding/json"
)

const (
	maxUserTokens = 2048
)

// UserEndpoint covers Bitly User endpoints
type UserEndpoint endpoint

// TODO: Add description to User, UserEmail and its fields
type User struct {
	DefaultGroupGuid string      `json:"default_group_guid,omitempty"`
	Name             string      `json:"name,omitempty"`
	Created          string      `json:"created,omitempty"`
	IsActive         bool        `json:"is_active,omitempty"`
	Modified         string      `json:"modified,omitempty"`
	IsSSOUser        bool        `json:"is_sso_user,omitempty"`
	Is2FAEnabled     bool        `json:"is_2fa_enabled,omitempty"`
	Login            string      `json:"login,omitempty"`
	Emails           []UserEmail `json:"emails,omitempty"`
}

type UserEmail struct {
	IsPrimary  bool   `json:"is_primary,omitempty"`
	IsVerified bool   `json:"is_verified,omitempty"`
	Email      string `json:"email,omitempty"`
}

func (u *User) ParseJSON(js []byte) error {
	return json.Unmarshal(js, u)
}

func (ep *UserEndpoint) Get(ctx context.Context) (user *User, resp *Response, err error) {
	req, err := ep.client.NewRequest("GET", "user", nil)

	if err != nil {
		return nil, nil, err
	}

	user = new(User)
	resp, err = ep.client.Do(ctx, req, user)
	if err != nil {
		return nil, resp, err
	}
	return user, resp, nil
}

func (ep *UserEndpoint) Update(ctx context.Context, user *User) (*User, *Response, error) {
	req, err := ep.client.NewRequest("PATCH", "user", nil)

	if err != nil {
		return nil, nil, err
	}

	user = new(User)
	resp, err := ep.client.Do(ctx, req, user)
	if err != nil {
		return nil, resp, err
	}
	return user, resp, nil

}
